# DrawSQL Clone

## How to run

```bash
npm start
```

## Data model

```mermaid
erDiagram

Diagram {
  id int PK
  name string
}
Diagram ||--o{ Table : has

Table {
  id int PK
  name string
  diagramId int FK
}
Table ||--o{ Index : has
Table ||--o{ Column : has

Index {
  id int PK
  columnId int FK
  tableId int FK
}
Index ||--|| Column : for

Column {
  id int PK
  name string
  dataType string
  tableId int FK
  foreignColumnId int FK
}
Column |o--o| Column : related
```
